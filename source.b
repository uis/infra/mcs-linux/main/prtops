/********************************************************************
*                                                                   *
*                              PRTOPS                               *
*                                                                   *
*         A program to convert text files, suitable for printing    *
*         on a lineprinter, to PostScript for A4 sheets.            *
*                                                                   *
* Copyright (c) University of Cambridge, 1990                       *
*                                                                   *
* Author - S. Kearsey, Cambridge University Computing Service       *
* Last Updated - October 1990                                       *
*                                                                   *
*-------------------------------------------------------------------*
* Permission is granted to anyone to use this software for any      *
* purpose on any computer system, and to redistribute it freely,    *
* subject to the following restrictions:                            *
*                                                                   * 
* 1. This software is distributed in the hope that it will be       *
*    useful, but WITHOUT ANY WARRANTY; without even the implied     *
*    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR        *
*    PURPOSE.                                                       *
*                                                                   * 
* 2. The origin of this software must not be misrepresented, either *
*    by explicit claim or by omission.                              *
*                                                                   * 
* 3. Altered versions must be plainly marked as such, and must not  *
*    be misrepresented as being the original software.              *
********************************************************************/

get "/usr/local/lib/libhdr"

global $(
         page.started   : FG    ; top.of.page    : FG+1
         current.x      : FG+2  ; current.y      : FG+3
         left.margin    : FG+4  ; right.margin   : FG+5
         top.margin     : FG+6  ; bottom.margin  : FG+7
         npage          : FG+8  ; line.spacing   : FG+9
         ccs.present    : FG+10 ; v.stream       : FG+11
         i.stream       : FG+12 ; o.stream       : FG+13
         storevec       : FG+14 ; storep         : FG+15
         font.name      : FG+16 ; font.size      : FG+17
         rcode          : FG+18 ; keytable       : FG+19
         cbuf           : FG+20 ; cbufp          : FG+21
         pwidth         : FG+22 ; pheight        : FG+23
         side.margin    : FG+24 ; head.margin    : FG+25
         string.buf     : FG+26 ; orientation    : FG+27
         page.layout    : FG+28 ; page.seq       : FG+29
         obuf           : FG+30 ; obufp          : FG+31
         nA4page        : FG+32 ; olinerem       : FG+33
         flist          : FG+34 ; nf             : FG+35
         $)

manifest $(
         STORESIZE = 1000                                            // internal store size

         CBUFSIZE = 64                                               // character buffer size
         OBUFSIZE = 255                                              // output buffer size (bytes)

         MAXSTRINGLEN = 255                                          // max no of chars in strings
         MAXISEGLEN   = 252                                          // max no of character in input record segment
         MAXOLINELEN  = 80                                           // max no of chars in output line

         DEFAULT.FOUNTSZ = 10 ; DEFAULT.LINESP  = 12                 // default parameter values
         DEFAULT.SMARGIN = 56 ; DEFAULT.HMARGIN = 56

         N.VALUE = 0 ; N.LINK = 1 ; N.TEXT = 2                       // key node structure

         TK.TYPE = 0 ; TK.VALUE = 1                                  // lexical token structure

         T.UNKNOWN = 0 ; T.NAME    = 1 ; T.INT     = 2               // token values
         T.FLOATPT = 3 ; T.STRING  = 4
         T.EOF     = 10; T.SCOLON  = 11; T.EQUALS  = 12

         K.UNKNOWN = 0 ; K.FOUNT   = 1 ; K.FSIZE   = 2               // keyword values
         K.LINESP  = 3 ; K.SMARGIN = 4 ; K.HMARGIN = 5
         K.A4TOA5  = 6 ; K.LANDSCP = 7 ; K.PORTRAIT= 8
         K.A4      = 9

         L.CC = 0 ; L.TEXT = 1                                       // line record structure

         A4.WIDTH = 594 ; A4.HEIGHT = 840                            // paper dimensions (points)

         O.PORTRAIT = 0  ; O.LANDSCAPE = 1                           //text orientation

         PL.A4      = 0  ; PL.2A5      = 1                           //page layout
         $)

let start() be $(                                                    // dummy start routine
    let v = vec STORESIZE                                            // working store vector

    storevec := v                                                    // set up pointer to store
    storep   := storevec + STORESIZE                                 // next free word (end to beginning)
    prtops()                                                         // enter main routine
    $)


and prtops() be $(                                                   // PRTOPS main routine
    let line = vec 1                                                 // data structure for a record
    let line.str = vec 64                                            // buffer for text of record

    line!L.TEXT := line.str                                          // initialise record structure

    initialise()                                                     // initialise program
    header()                                                         // write PostScript header
    prologue()                                                       // write PostScript prologue
    while get.file() $(                                              // open next file
        while get.line(line) $(                                      // get a line from input file
            switchon line!L.CC into $(                               // branch on its carriage control
              case '-':                                              // 2 blank lines
                line.feed()
              case '0':                                              // 1 blank line
                line.feed()
              case '*s': default:                                    // next line
                line.feed()
              case '+':                                              // overprint
                carriage.return()
                put.line(line!L.TEXT)                                // output line
                endcase
              case '1':                                              // next page
                new.page()
                put.line(line!L.TEXT)                                // output line
                endcase
              $)
            $)
        $)
    trailer()                                                        // write PostScript trailer
    flush.obuf()                                                     // flush out remaining output
    report.info()                                                    // report document information
    stop(rcode)                                                      // stop with current return code
    $)


and initialise() be $(                                               // perform initialisation

    rcode := 0                                                       // default return code

    getfileargs()                                                    // get list of files
    o.stream := stdout                                               // output file stream
    v.stream := stderr                                               // verification file stream
    if o.stream=0 report.error("No output file",8)                   // check stream OK

    initcbuf()                                                       // initialise opt input buffer
    initobuf()                                                       // initialise output buffer
    initsbuf()                                                       // initialise string buffer
    initkeys()                                                       // initialise keywords

    pwidth       := A4.WIDTH                                         // default paper width
    pheight      := A4.HEIGHT                                        // default paper height
    orientation  := O.PORTRAIT                                       // default text orientation
    page.layout  := PL.A4                                            // default page layout
    font.size    := DEFAULT.FOUNTSZ                                  // default font size
    line.spacing := DEFAULT.LINESP                                   // default linespacing
    side.margin  := DEFAULT.SMARGIN                                  // default side margin
    head.margin  := DEFAULT.HMARGIN                                  // default head margin
    font.name    := newvec(MAXSTRINGLEN)                             // get store for font name
    insert.bytes("Courier",0,8,font.name,0)                          // name of font used
    page.started := FALSE                                            // page started flag
    top.of.page := TRUE                                              // first-line-on-page flag
    npage    := 0                                                    // current logical page number
    page.seq := 1                                                    // page sequence number
    nA4page  := 0                                                    // current number of A4 sheets printed

    get.opts()                                                       // set up options required

    left.margin  := side.margin                                      // left margin position
    right.margin := pwidth - side.margin                             // right margin position
    top.margin   := pheight - head.margin                            // top margin position
    bottom.margin:= head.margin                                      // bottom margin position

    selectoutput(o.stream)                                           // select PostScript output stream
    ccs.present := TRUE                                              // ccs always present on UNIX
    $)


and getfileargs() be $(                                              // get list of files from command args
    let endopt = 1

    for i=argc-1 to 1 by -1 $(                                       // work back along args
       if (argv!i)%1 = '-' & (argv!i)%0 ~= 1 then $(                 // if get to an option keyword
          endopt := i + 2                                            // point to first file beyond it
          break                                                      // stop loop
          $)
       $)
    nf := argc - endopt                                              // number of file args
    if nf>0 then $(                                                  // if there are some
       flist := getvec(nf-1)                                         // get store for list
       if flist=0 then $(                                            // can't get list store
          report.error("insufficient store available",8)             // report error
          $)
       for i=0 to nf-1 $(                                            // copy list of file args
          flist!i := argv!(endopt+i)
          $)
       $)
    $)


and get.file() = valof $(                                            // selects next input file
    static $( file = 0 $)
    let done = FALSE

    if file=0 | file<nf then $(                                      // if first time, or all files not processed
        test nf=0 then $(                                            // if no files use stdin
            i.stream := stdin
            $)
        else test (flist!file)%0=1 & (flist!file)%1='-' then $(      // if '-' use stdin
            i.stream := stdin
            $)
        else $(
            i.stream := findinput(flist!file)                        // find file
            $)
        if i.stream=0 report.error("No input file",8)                // check stream OK
        selectinput(i.stream)                                        // select text input stream
        file := file + 1
        done := TRUE
        $)
    resultis done                                                    // returns TRUE if file opened
    $)                                                               //         FALSE if not


and initcbuf() be $(                                                 // initialise option stream input buffer

    cbuf := getvec(CBUFSIZE)                                         // get buffer
    cbufp := 0                                                       // set buffer pointer
    if cbuf=0 then $(                                                // can't get buffer store
        report.error("insufficient store available",8)               // report error
        $)
    for i=0 to CBUFSIZE $(                                           // initialise contents to zeros
        cbuf!i := 0
        $)
    $)


and initobuf() be $(                                                 // initialise output line buffer

    obuf := getvec(OBUFSIZE/4)                                       // get buffer
    if obuf=0 then $(                                                // if can't get store
        report.error("insufficient store available",8)               // report error
        $)
    obuf%0   := 0                                                    // empty buffer
    obufp    := 1                                                    // pointer to next free entry
    olinerem := MAXOLINELEN                                          // current line empty
    $)


and initsbuf() be $(                                                 // initialise string buffer

    string.buf := getvec(MAXSTRINGLEN/4)                             // get vector to hold max string
    if string.buf=0 then $(                                          // if can't get store
        report.error("insufficient store available",8)               // report error
        $)
    string.buf%0 := 0                                                // null string
    $)


and initkeys() be $(                                                 // set up table of option keywords

    keytable := 0                                                    // initialise key table
    defkey(K.FOUNT,"FOUNT")                                          // define keywords
    defkey(K.FOUNT,"FONT")
    defkey(K.FSIZE,"FSIZE")
    defkey(K.LINESP,"LSPACE")
    defkey(K.SMARGIN,"SIDE")
    defkey(K.HMARGIN,"HEAD")
    defkey(K.A4,"A4")
    defkey(K.A4TOA5,"A4TOA5")
    defkey(K.LANDSCP,"LANDSCAPE")
    defkey(K.PORTRAIT,"PORTRAIT")
    $)


and defkey(value,text) be $(                                         // enter key into key table
    let wordsize = (text%0)/4                                        // number of words in string, less one
    let node = newvec(N.TEXT+wordsize)                               // get vector for new node
    let ntext = 0

    node!N.LINK  := keytable                                         // point to current beginning of table
    node!N.VALUE := value                                            // enter key value
    ntext := node + N.TEXT                                           // point to text area of node
    for i=0 to text%0 $(                                             // copy text of key into node
        ntext%i := text%i
        $)
    keytable := node                                                 // new node is now beginning of table
    $)


and get.opts() be $(                                                 // fetch options
    manifest $( long = slct 32:0:0 $)
    external $( getopt : "getopt"
                optind : "optind"  $)
    let opt = ?
    let token = vec 1                                                // vector for token

    opt := getopt(argc,argv.c,"p:f:"*4+1)
    switchon opt into $(
      case 'p':
        selectinput(findinstring(argv!((long in optind)-1)))
        endcase
      case 'f':
      default:
        return
        endcase
      $)
    while gettok(token)~=T.EOF $(                                    // get next token until EOF
        switchon token!TK.TYPE into  $(                              // branch on token type
          case T.NAME: $(                                            // alphanumeric name
            let name = token!TK.VALUE                                // pointer to name
            let key = lookupkey(name)                                // look it up in keyword table

            switchon key into $(                                     // branch on key value
              case K.FOUNT: $(                                       // set fount used
                let fname = get.string()                             // get fount name

                insert.bytes(fname,0,(fname%0)+1,font.name,0)        // store it
                $) endcase
              case K.FSIZE:                                          // set fount size
                font.size := get.integer()
                endcase
              case K.LINESP:                                         // set line spacing
                line.spacing := get.integer()
                endcase
              case K.SMARGIN:                                        // set side margins
                side.margin := get.integer()
                endcase
              case K.HMARGIN:                                        // set head and tail margins
                head.margin := get.integer()
                endcase
              case K.A4:                                             // A4 page on A4 sheet
                page.layout := PL.A4
                endcase
              case K.A4TOA5:                                         // two A5 pages on a A4 sheet
                page.layout := PL.2A5
                endcase
              case K.LANDSCP:                                        // landscape orientation
                orientation := O.LANDSCAPE
                pwidth  := A4.HEIGHT                                 // reset paper dimensions
                pheight := A4.WIDTH
                endcase
              case K.PORTRAIT:                                       // portrait orientation
                orientation := O.PORTRAIT
                pwidth  := A4.WIDTH                                  // reset paper dimensions
                pheight := A4.HEIGHT
                endcase
              case K.UNKNOWN:                                        // unknown keyword
                report.opterr("unknown option word %s",8,name)       // report option error
                endcase
              $)
            $)
            endcase
          case T.SCOLON:                                             // ignore semi-colons
            endcase
          default:                                                   // anything else
            report.opterr("word expected",8)                         // report option error
            endcase
          $)
        $)
    $)


and get.string() = valof $(                                          // gets string argument
    let str = 0
    let token = vec 1
    let ttype = gettok(token)                                        // next token

    if ttype=T.EQUALS then ttype := gettok(token)                    // if '=' get next token
    test ttype=T.STRING then $(                                      // if a string
        str := token!TK.VALUE                                        // extract pointer
        $)
    else $(                                                          // if not
        report.opterr("string expected",8)                           // report error
        $)
    resultis str                                                     // return pointer to string
    $)


and get.integer() = valof $(                                         // gets integer argument
    let num = 0
    let token = vec 1
    let ttype = gettok(token)                                        // next token

    if ttype=T.EQUALS then ttype := gettok(token)                    // if '=' get next token
    test ttype=T.INT then $(                                         // if an integer
        num := token!TK.VALUE                                        // extract value
        $)
    else $(                                                          // if not
        report.opterr("number expected",8)                           // report error
        $)
    resultis num                                                     // return integer value
    $)


and gettok(token) = valof $(                                         // get next lexical token from input
    let type  = T.UNKNOWN                                            // token type
    let value = 0                                                    // token value
    let str   = string.buf                                           // buffer for strings
    let num   = 0                                                    // numerical values
    let ch    = rch()                                                // next character of input

    while ch='*s'|ch='*P'|ch='*N'|ch='*T' do ch := rch()             // skip spaces,newpage,newline and tabs
    switchon uc(ch) into $(                                          // branch on character (uppercase letters)
      case 'A': case 'B': case 'C': case 'D': case 'E':              // letter A-Z (also a-z as uppercased)
      case 'F': case 'G': case 'H': case 'I': case 'J':
      case 'K': case 'L': case 'M': case 'N': case 'O':
      case 'P': case 'Q': case 'R': case 'S': case 'T':
      case 'U': case 'V': case 'W': case 'X': case 'Y':
      case 'Z':
        rdname(str,ch)                                               // read in an alphanumeric name
        type  := T.NAME                                              // name token
        value := str                                                 // token value is pointer to string
        endcase
      case '0': case '1': case '2': case '3': case '4':              // digit 0-9
      case '5': case '6': case '7': case '8': case '9':
        type  := rdnumber(@num,ch)                                   // read a number (integer or floating pt.)
        value := num                                                 // token value is number
        endcase
      case '*"': case '*'':
        rdstring(str,ch)
        type  := T.STRING
        value := str
        endcase
      case ';':                                                      // semicolon
        type  := T.SCOLON
        value := ch
        endcase
      case '=':                                                      // equals
        type  := T.EQUALS
        value := ch
        endcase
      case ENDSTREAMCH:                                              // end of input stream
        type  := T.EOF
        value := ch
        endcase
      default:                                                       // anything else is unrecognised
        type  := T.UNKNOWN
        value := ch
        endcase
      $)
    token!TK.TYPE  := type                                           // set up token record
    token!TK.VALUE := value
    resultis type                                                    // return with type of token found
    $)


and rdname(str,ch1) be $(                                            // read a word of alphanumeric chars
    let nchar = 1
    let ch = rch()                                                   // get next character

    str%nchar := ch1                                                 // put firt character away
    while alphanum(ch) $(                                            // while next character is alphanumeric
        nchar +:= 1                                                  // increment character count
        str%nchar := ch                                              // put character away
        ch := rch()                                                  // get next character
        $)
    unrch()                                                          // put non-alphanumeric char back on input
    str%0 := nchar                                                   // put string size in
    $)


and rdstring(str,dlm) be $(                                          // reads a string
    let nchar = 0
    let ch = rch()                                                   // get next character

    until ch=dlm | ch='*n' | ch=ENDSTREAMCH $(                       // until delimiter read or end of line or file
        nchar +:= 1                                                  // increment character count
        str%nchar := ch                                              // put character in string
        ch := rch()                                                  // get next character
        $)
    str%0 := nchar                                                   // put string length in
    $)


and rdnumber(num.addr,ch1) = valof $(                                // reads a number (integer or floating pt.)
    let d = 0
    let num = 0
    let ch = ch1

    $(  switchon ch into $(                                          // branch on character
          case '0': case '1': case '2': case '3': case '4':          // digit 0-9
          case '5': case '6': case '7': case '8': case '9':
            d := ch - '0'                                            // numerical equivalent of digit
            num := num*10 + d                                        // add to current partial result
            endcase
          default:                                                   // anything else
            break                                                    // stop getting characters
            endcase
          $)
        ch := rch()                                                  // get next character
        $) repeat
    unrch()                                                          // put terminating char back to input
    !num.addr := num                                                 // return number
    resultis T.INT                                                   // return T.INT    if integer
    $)                                                               //        T.FLOATP if floating point


and alphanum(ch) = valof $(                                          // tests if ch is alphameric
    let alpha = FALSE

    switchon uc(ch) into $(                                          // branch on uppercased character
      case 'A': case 'B': case 'C': case 'D': case 'E':              // letter A-Z (also a-z as uppercased)
      case 'F': case 'G': case 'H': case 'I': case 'J':
      case 'K': case 'L': case 'M': case 'N': case 'O':
      case 'P': case 'Q': case 'R': case 'S': case 'T':
      case 'U': case 'V': case 'W': case 'X': case 'Y':
      case 'Z':
      case '0': case '1': case '2': case '3': case '4':              // digit 0-9
      case '5': case '6': case '7': case '8': case '9':
         alpha := TRUE                                               // alphameric
         endcase
      default:                                                       // anything else
         alpha := FALSE                                              // not alphameric
         endcase
      $)
    resultis alpha                                                   // returns TRUE  if alphameric
    $)                                                               //         FALSE if not


and lookupkey(text) = valof $(                                       // look up key value in table of keywords
    let node = keytable                                              // first node in table
    let ktext,nchars = 0,0
    let match = FALSE

    until node=0 | match $(                                          // until at end of table or key matched
        ktext  := node + N.TEXT                                      // point to key text
        nchars := ktext%0                                            // number of characters in key
        test text%0=nchars then $(                                   // if key and text are same size
            match := TRUE                                            // there is a potential match
            for i=1 to nchars $(                                     // for each character in key
                if uc(text%i)~=ktext%i $(                            // if uppercased char doesn't match
                    match := FALSE                                   // no match
                    node := node!N.LINK                              // get next node
                    break                                            // stop checking current key
                    $)
                $)
            $)
        else $(                                                      // key and text different lengths
            node := node!N.LINK                                      // get next node
            $)
        $)
    if node=0 then resultis K.UNKNOWN                                // if no match in table return K.UNKNOWN
    resultis node!N.VALUE                                            // return key value
    $)


and header() be $(                                                   // PostScript file header

    wchars("%!PS-Adobe-2.0*n")
    wchars("%!needs a4*n")                                           // local convention (used by PSOUT)
    wchars("%%Title: Line Printer File*n")
    wchars("%%Creator: PRTOPS*n")
    wchars("%%Pages: (atend)*n")
    wchars("%%EndComments*n")
    $)


and prologue() be $(                                                 // PostScript prologue -
    let str = vec 10                                                 //     definitions and set-up
    let offset = 0

    wchars("a4*n")                                                   // A4 fix-up (origin bottom left corner)
//  wchars("statusdict /waittimeout 200 put*n")                      // lengthen timeouts for line delays
    str%0 := 0
    addstr(str,"/")
    addstr(str,font.name)
    addstr(str," findfont ")
    wchars(str)                                                      // find fount requested
    itoc(font.size,str)
    addstr(str," scalefont setfont*n")
    wchars(str)                                                      // select required size
    wchars("/mt {moveto} def /s {show} def*n")                       // aliases for operations used
    wchars("/window { /y2 exch def /y1 exch def /x2 exch def /x1 exch def*n") // define window operation
    wchars("newpath x1 y1 moveto x2 y1 lineto x2 y2 lineto x1 y2 lineto*n")   // path round border
    wchars("closepath clip} def*n")
    wchars("%%EndProlog*n")
    $)


and trailer() be $(                                                  // PostScript Trailer
    let str = vec 10
    let num = vec 3

    if page.started then flush.lastpage()                            // end current A4 page if one was started
    wchars("%%Trailer*n")
    str%0 := 0                                                       // null string
    addstr(str,"%%Pages: ")                                          // build string "%%Pages: n*n"
    itoc(nA4page,num)
    addstr(str,num)
    addstr(str,"*n")
    wchars(str)                                                      // total number of pages in document
    $)


and new.page() be $(                                                 // moves to next page

    if page.started then end.page()                                  // end current page if there is one
    top.of.page := TRUE                                              // set first-line flag
    $)


and get.line(line) = valof $(                                        // get line of input text
    static $( seg.no = 0 ; wrap.warn.flag = FALSE $)                 // record segment count & wrapping flag
    let eol  = FALSE                                                 // end-of-line-obtained flag
    let cc   = line+L.CC                                             // carriage control character
    let text = line!L.TEXT                                           // line text
    let record = vec MAXISEGLEN/4+1                                  // input buffer
    let nchars,start = 0,0

    nchars := readrec(record)                                        // read record
    if nchars<-1 & wrap.warn.flag=FALSE then $(                      // first time record segmented
        report.error("overlong input lines wrapped",4)               // issue warning
        wrap.warn.flag := TRUE
        $)
    if nchars=ENDSTREAMCH resultis FALSE                             // if end-of-file, return
    test ccs.present then $(                                         // are carriage controls present?
        if nchars>=0 then eol := TRUE                                // if end-of-line segment, set flag
        nchars := abs(nchars)                                        // make nchars positive
        seg.no +:= 1                                                 // increment segment count
        test seg.no=1 & nchars>0 then $(                             // if first segment of line and not empty
            !cc := record%0                                          // pick off carriage control
            nchars -:= 1                                             // number of chars in text
            start := 1                                               // offset to start of text
            $)
        else $(                                                      // not the first segment
            !cc := '*s'                                              // wrap to next line
            start := 0                                               // offset to start of text
            $)
        if eol then seg.no := 0                                      // reset segment count if eol obtained
        $)
    else $(                                                          // no cc's present
        !cc := '*s'                                                  // move to next line
        start := 0                                                   // offset to start of text
        nchars := abs(nchars)                                        // make nchars positive
        $)
    for i=1 to nchars $(                                             // copy record text to line
        text%i := record%(start+i-1)
        $)
    text%0 := nchars                                                 // number of chars in text
    resultis TRUE                                                    // returns TRUE  if line obtained
    $)                                                               //         FALSE if eof


and put.line(line) be $(                                             // put line out (convert to PostScript)
    let ch  = 0
    let str = vec 3

    if top.of.page then $(                                           // if first line on page
        start.page()                                                 // put out PostScript page header
        top.of.page := FALSE                                         // unset flag
        $)
    if line%0>0 then $(                                              // if line not empty
        itoc(current.x,str) ; wchars(str) ; wch(' ')                 // convert x to string and output
        itoc(current.y,str) ; wchars(str) ; wch(' ')                 // convert y to string and output
        wchars("mt (")                                               // move and begin string
        for i=1 to line%0 $(                                         // for each character
            ch := line%i                                             // extract character
            convert.char(ch,str)                                     // convert character to PostScript string
            if (olinerem<=str%0) then $(                             // if last character or escaped sequence
                wchars("\*n")                                        // nullify newline with an escape '\'
                $)
            wchars(str)                                              // write character (or sequence) out
            $)
        wchars(") s*n")                                              // end string and set it- newline
        $)
    $)

and line.feed() be $(                                                // move down a line

    unless top.of.page $(                                            // ignore if at top of new page
        current.y -:= line.spacing                                   // move current y position down
        if (current.y-line.spacing/2)<bottom.margin  then $(         // if off bottom
            new.page()                                               // go to next page
            $)
        $)
    $)


and carriage.return() be $(                                          // move to left margin of page

    current.x := left.margin                                         // still on same line
    $)


and start.page() be $(                                               // start a new page off

    page.started := TRUE                                             // a page has now been started
    npage +:= 1                                                      // increment page counter
    current.x := left.margin                                         // move to upper left corner of page
    current.y := top.margin - line.spacing
    if page.seq=1 then $(                                            // if first page on A4 sheet
        nA4page +:= 1                                                // increment A4 sheet count
        A4page.header()                                              // write PostScript A4 page header
        $)
    subpage.header(page.seq)                                         // write PostScript subpage header
    $)


and end.page() be $(                                                 // end current page

    subpage.trailer()                                                // write PostScript subpage trailer
    page.seq := next.subpage(page.seq)                               // get sequence number of next page
    if page.seq=1 then $(                                            // if next starts a new A4 sheet
        A4page.trailer()                                             // write PostScript A4 page trailer
        $)
    $)


and flush.lastpage() be $(                                           // end last subpage and flush A4 sheet

    subpage.trailer()                                                // write PostScript subpage trailer
    A4page.trailer()                                                 // write PostScript A4 page trailer
    $)


and subpage.header(seq) be $(                                        // PostScript for subpage start
    let str = vec 3
    let page.offset = 0

    switchon page.layout into $(                                     // branch on page layout
      case PL.A4:                                                    // single A4 pages
        page.offset := 0                                             // no offset to page (could add odd/even
        endcase                                                      // offsets later)
      case PL.2A5:                                                   // two A5 pages on an A4 sheet
        switchon seq into $(                                         // branch on page sequence number
          case 1:                                                    // first subpage on sheet
            page.offset := 0                                         // no offset
            endcase
          case 2:                                                    // second subpage on sheet
            page.offset := A4.WIDTH                                  // offset by A4 page width (pre-scaled units)
            endcase
          $)
        endcase
      $)
    wchars("save*n")                                                 // save current VM state
    itoc(page.offset,str) ; wchars(str)                              // apply page offset
    wchars(" 0 translate*n")
    if orientation=O.LANDSCAPE then $(                               // if landscape orientation
        itoc(pheight,str) ; wchars(str)                              // rotate user coordinates
        wchars(" 0 translate [0 1 -1 0 0 0] concat*n")               // [0 1 -1 0 0 0] matrix preferred because
        $)                                                           //   more accurate - allows LaserWriter to
                                                                     //   recognise aligned clipping rectangle
                                                                     //   in device coordinates and use
                                                                     //   optimised clipping algorithms (five
                                                                     //   times faster).
    itoc(left.margin,str)   ; wchars(str) ; wch(' ')                 // set up windowing at margins
    itoc(right.margin,str)  ; wchars(str) ; wch(' ')
    itoc(bottom.margin,str) ; wchars(str) ; wch(' ')
    itoc(top.margin,str)    ; wchars(str) ; wch(' ')
    wchars("window*n")
    $)


and subpage.trailer(seq) be $(                                       // PostScript for subpage end

    wchars("restore*n")                                              // restore VM state
    $)


and next.subpage(seq) = valof $(                                     // returns next sequence number
    let next = 0

    switchon page.layout into $(                                     // branch on page layout
      case PL.A4:                                                    // single A4 pages
        next := 1                                                    // always first on page
        endcase
      case PL.2A5:                                                   // two A5 pages on an A4 sheet
        next := (seq rem 2) + 1                                      // cycle through [1],[2]
        endcase
      $)
    resultis next                                                    // return sequence number of next page
    $)


and A4page.header() be $(                                            // PostScript for A4 page start
    let str = vec 3

    wchars("%%Page:? ")                                              // sheet number
    itoc(nA4page,str) ; wchars(str) ; wch('*n')
    if page.layout=PL.2A5 then $(                                    // if printing A4 on A5
        itoc(A4.WIDTH,str) ; wchars(str)                             // use landscape
        wchars(" 0 translate [0 1 -1 0 0 0] concat*n")               // matrix preferred (see subpage.header)
        wchars("1 2 sqrt div dup scale*n")                           // and scale by 1/sqrt(2.0)
        $)
    $)


and A4page.trailer() be $(                                           // PostScript for page end

    wchars("showpage*n")                                             // print page
    $)


and convert.char(ch,str) be $(                                       // make char suitable for PostScript string
    let nchars,offset = 0,1

    switchon ch into $(                                              // branch on character
      case '*b':                                                     // backspace change to \b
        offset := insert.bytes("\b",1,2,str,offset)
        endcase
      case ')': case '(': case '\':                                  // (,),\  become \(,\),\\
        offset := insert.bytes("\",1,1,str,offset)
      default:                                                       // all the rest
        str%offset := ch                                             // add character to string
        offset +:= 1
        endcase
      case 163:                                                      // pound si
        offset := insert.bytes("\243",1,4,str,offset)
        endcase
        $)
    nchars := offset-1                                               // number of chars in string
    str%0 := nchars                                                  // string length
    $)


and itoc(int,str) = valof $(                                         // convert integer to decimal char. format
    let t = vec 20
    let intval,i,d = abs(int),0,0
    let digits = table '0','1','2','3','4','5','6','7','8','9'

    $( i := i + 1                                                    // pick off digits from r to l
       d := intval rem 10
       t!i := digits!d
       intval := intval/10
       $) repeatuntil intval=0
    if int<0 then $(
       i := i + 1
       t!i := '-'                                                    // add minus sign
       $)
    str%0 := i
    for j=1 to i                                                     // reverse characters into string
        str%j := t!(i-j+1)
    resultis i                                                       // return number of characters
    $)


and addstr(str1,str2) be $(                                          // adds string 2 to end of string 1

    str1%0 := insert.bytes(str2,1,str2%0,str1,(str1%0)+1) - 1        // insert bytes and update string length
    $)


and insert.bytes(f.str,f.offset,nbytes,t.str,t.offset) = valof $(    // insert part of string into another

    for i=0 to nbytes-1 $(                                           // copy bytes
        t.str%(t.offset+i) := f.str%(f.offset+i)
        $)
    resultis t.offset + nbytes                                       // return next offset in 'to' string
    $)


and newvec(n) = valof $(                                             // allocate new vector from internal store

    storep := storep - n - 1                                         // move pointer to next free word
    if storep<storevec then $(                                       // if off beginning of store vector
        report.error("Insufficient internal store",8)                // give error
        $)
    resultis storep+1                                                // return pointer to new vector
    $)


and rch() = valof $(                                                 // read next character of input
    let ch = rdch()                                                  // read character

    cbufp +:= 1                                                      // increment pointer in buffer
    cbufp := cbufp rem CBUFSIZE                                      // if off end - back to beginning
    cbuf!cbufp := ch                                                 // store character in buffer
    resultis ch                                                      // return character
    $)


and unrch() be $(                                                    // put character back on input

    unrdch()                                                         // unread character
    cbufp -:= 1                                                      // move buffer pointer back
    if cbufp<0 then cbufp +:= CBUFSIZE                               // if off front
    $)                                                               // move to end


and readrec(rbuff) = valof $(                                        // read in a record from the current input
    static $( cc = '*s' ; seg = FALSE $)                             //   sets 1st char in rbuff to carriage control
    let done = FALSE                                                 //           space   -   newline
    let ch = ?                                                       //             +     -   carriage return
    let n = 0                                                        //             1     -   newpage

    until done then $(                                               // until finished
       if n=MAXISEGLEN then $(                                       // if buffer full
           n := -n
           seg := TRUE
           break                                                     // stop reading
           $)
       ch := rdch()                                                  // get next character
       switchon ch into $(                                           // branch on character
         case '*c':                                                  // carriage return <cr>
           if ~seg then $(                                           // if not following segment
               rbuff%0 := cc                                         // put in carriage control
               n := n + 1
               $)
           cc := '+'                                                 // save cc for next line
           seg := FALSE                                              // end of segmentation
           done := TRUE
           endcase
         case '*n':                                                  // newline <lf>
           if ~seg then $(                                           // if not following segment
               rbuff%0 := cc                                         // put in carriage control
               n := n + 1
               $)
           cc := '*s'                                                // save cc for next line
           seg := FALSE                                              // end of segmentation
           done := TRUE
           endcase
         case '*p':                                                  // newpage <ff>
           if ~seg then $(                                           // if not following segment
               rbuff%0 := cc                                         // put in carriage control
               n := n + 1
               $)
           cc := '1'                                                 // save cc for next line
           seg := FALSE                                              // end of segmentation
           done := TRUE
           endcase
         case ENDSTREAMCH:                                           // end of file
           test n>0 then $(                                          // if characters already read
              if ~seg then $(                                        // and not following segment
                  rbuff%0 := cc
                  n := n + 1
                  $)
              cc := '*s'                                             // treat as if newline <lf>
              $)
           else $(                                                   // bare eof
              n := -1
              $)
           seg := FALSE
           done := TRUE
           endcase
         default:                                                    // data characters
           test seg then $(                                          // if following segment
               rbuff%n := ch                                         // put char in buffer
               $)
           else $(                                                   // if lst seg or not segmented
               rbuff%(n+1) := ch                                     // put char in buffer offset by 1
               $)
           n := n + 1                                                // increment character count
           endcase
         $)
       $)
    resultis n                                                       // return n>=0 - end of record read
    $)                                                               //        n=-1 - end of file
                                                                     //        n<-1 - record segmented

and uc(ch) = valof $(                                                // uppercase any letters (a-z)

    switchon ch into $(                                              // branch on character
      case 'a': case 'b': case 'c': case 'd': case 'e':              // lower case letter
      case 'f': case 'g': case 'h': case 'i': case 'j':
      case 'k': case 'l': case 'm': case 'n': case 'o':
      case 'p': case 'q': case 'r': case 's': case 't':
      case 'u': case 'v': case 'w': case 'x': case 'y':
      case 'z':
        ch := ch - 'a' + 'A'                                         // upper case it (assumes same sequence
        endcase                                                      // for A-Z as a-z)
      default:                                                       // leave rest alone
        endcase
      $)
    resultis ch                                                      // return character
    $)


and wlex(str) be $(                                                  // outputs a lexeme (must not wrap)

    if olinerem<str%0 then $(                                        // if string to long for rest of line
        wch('*n')                                                    // start new line
        $)
    wchars(str)                                                      // output string
    $)


and wchars(str) be $(                                                // outputs a string of characters
                                                                     // they may be wrapped to next line
    for i=1 to str%0 $(                                              // output each character
        wch(str%i)
        $)
    $)


and wch(ch) be $(                                                    // outputs a character

    test ch='*n' then $(                                             // if newline character
        olinerem := MAXOLINELEN                                      // reset line remaining count
        $)
    else $(                                                          // if anything else
        test olinerem=0 then $(                                      // if current line full
            wch('*n')                                                // start a new line
            $)
        else $(                                                      // if line not full
            olinerem -:= 1                                           // decrement char remaining count
            $)
        $)
    if obufp>=OBUFSIZE then $(                                       // if output buffer full
        flush.obuf()                                                 // write buffer out
        $)
    obuf%obufp := ch                                                 // add character to buffer
    obuf%0    +:= 1                                                  // increment buffer count and pointer
    obufp     +:= 1
    $)


and flush.obuf() be $(                                               // flush output buffer

    writes(obuf)                                                     // write out buffer contents
    obufp  := 1                                                      // reset buffer parameters
    obuf%0 := 0
    $)


and report.info() be $(                                              // report information about output doc.
    let current.output = output()                                    // note current output
    let page.size = valof $( switchon page.layout into $(            // logical page size
                               case PL.2A5: resultis "A5"
                               case PL.A4:
                               default:     resultis "A4" $) $)

    selectoutput(v.stream)                                           // select verification output
    writef("Output: %n %s page(s) on %n A4 sheet(s), %npt %s, %npt spacing",  // write info
              npage,page.size,nA4page,font.size,font.name,line.spacing)
    writef(".*n")
    selectoutput(current.output)                                     // restore current output
    $)


and report.opterr(message,rc,p1,p2,p3) be $(                         // report syntax errors in opt string
    let current.output = output()                                    // note current output

    if rc>rcode then rcode := rc                                     // accumulate max rcode
    selectoutput(v.stream)                                           // select verification output
    test rcode>4 then $(                                             // if error
        writes("****PRTOPS Error - ")                                // put out error message
        writef(message,p1,p2,p3)
        wrch('*n')
        report.context()                                             // report context of error
        writef("PRTOPS abandoned*n")
        stop(rcode)                                                  // stop with current return code
        $)
    else $(                                                          // if only warning
        writes("****PRTOPS Warning - ")                              // put out warning message
        writef(message,p1,p2,p3)
        wrch('*n')
        report.context()                                             // report context of error
        selectoutput(current.output)                                 // restore current output
        $)
    $)


and report.context() be $(                                           // report context of syntax error
    let ch,p = 0,cbufp+2                                             // start beyond possible unrch() char

    for i=1 to CBUFSIZE-1 $(                                         // for each character in buffer
        if p>=CBUFSIZE then p := 0                                   // if off end of buffer move to beginning
        ch := cbuf!p                                                 // get character
        unless ch=0 $(                                               // unless not read in yet
            wrch(ch)                                                 // write out character
            unless ch='*n'|ch='*s'|ch='*p'|ch='*b'|ch='*t'           // unless control char
                cbuf!p := '*s'                                       // replace character with a space
            $)
        p +:= 1                                                      // point to next character
        $)
    wrch('*n')                                                       // newline
    cbuf!cbufp := '^'                                                // indicate last character with ^
    p := cbufp+2
    for i=1 to CBUFSIZE-1 $(                                         // put out pointer to last char read
        if p>=CBUFSIZE then p := 0
        ch := cbuf!p
        unless ch=0 then wrch(ch)
        p +:= 1
        $)
    wrch('*n')
    $)


and report.error(message,rc) be $(                                   // report error
    let current.output = output()                                    // note current output

    if rc>rcode then rcode := rc                                     // accumulate max rcode
    selectoutput(v.stream)                                           // select verification output
    test rcode>4 then $(                                             // if error
        writef("****PRTOPS Error - %s*n",message)                    // put out error message
        writef("PRTOPS abandoned*n")
        stop(rcode)                                                  // stop with current return code
        $)
    else $(                                                          // if only warning
        writef("****PRTOPS Warning - %s*n",message)                  // put out warning message
        selectoutput(current.output)                                 // restore current output
        $)
    $)
