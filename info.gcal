.flag \h1 "$scl$su" "$pu$pcl"
.flag \h2 "$su" "$pu"
.flag \h3 "$sb" "$pb"
.
.
$c \h1 Specification of PRTOPS \h1
.space 4
\h2 Author:\h2#S.#Kearsey $e ~%date
.section Introduction
PRTOPS is a program for converting plain-text files, which
may contain ANSI carriage controls, into POSTSCRIPT form suitable
for sending to an Apple LaserWriter.  A number of options are
provided allowing the user to alter the size and layout of the
resulting document pages.
.section User Interface
The PRTOPS command has the following syntax:
.display
PRTOPS [[FROM] <plain-text>] [TO <postscript>] [OPT <options>]
       [VER <verification>]
.endd
In addition, there are also the standard keywords \h3 STORE\h3,
\h3 TIME\h3, \h3 ERRLABEL \h3$$ and \h3 RCLEVEL\h3.
The \h3 FROM\h3 $$ keyword
is optional if the item comes first, and specifies the input
plain-text file.  If no input is specified, the current dataset
(%C) is used.
The \h3 TO\h3$$ keyword specifies the destination for the
PostScript output.  If omitted, the output is sent to the standard
output dataset (%O), which becomes the current data set at the
end of the command.
The \h3 VER\h3$$ keyword specifies the destination for the
verification output from the program.  If omitted, the output is
sent to the terminal dataset (*).
The \h3 OPT\h3$$ keyword specifies an options
string, the details of which are described below.  The string must
be enclosed in single quotes if it is to contain spaces.
Single quotes required in a quoted option string must be entered as
two consecutive single quotes.
.par
Some examples of calls to the PRTOPS command are:
.display
prtops
prtops .file
prtops &file to &pscript
prtops from .file to &pscript opt landscape;lspace=12
prtops opt 'a4toa5 fount "Times-Roman" fsize=8'
.endd
.section Option Syntax
An option string consists of a number of items separated by
semicolons.  Spaces are ignored except that they serve to
delimit words or numbers.  Each item consists of an option
name possibly followed by a number of values.  Option names
consist of letters and/or digits, the first of which must
be a letter.  Upper and lower case letters are synonymous.
Values following option names may be separated from them by
an equals sign or simply a space.  For example:
.display
fount="Helvetica";landscape
A4toA5;side=60;head=36
fsize 6; lspace 7
.endd
.section Options Available
The options available to modify the appearance of the final
document are as follows.
.space 1
\h3 Font\h3: This is followed by a string enclosed in matching
single or double quotes, and specifies the name of the fount
on the output device that the document is to be printed in.
If a proportionally spaced fount is chosen, then the relative
layout and length of lines will differ from a line-printer
listing of the original document.  To preserve the line-printer
appearance of a document a monospaced fount, such as "Courier"
(the default), should be used.   \h3 Fount\h3$$ may be used as a
synonym for \h3 font\h3.
.space 1
\h3 Fsize\h3: This is followed by an integer specifying the size,
in points, of the fount used to print the document.  Here and
elsewhere in this specification, a point is taken to be 1/72nd
of an inch.  The default fount size used is 10pt.
.space 1
\h3 Lspace\h3: This is followed by an integer specifying the
line spacing, in points, between lines in the document.  The
distance is taken to be that between the character base-lines
of consecutive lines of text.  The default line spacing is 12pt.
.space 1
\h3 Side\h3: This is followed by an integer specifying the width,
in points, of the side margins of each page of text.  The default
side margin is 56pt.  Note that, reducing the margin
below approximately 18pt may result in the edges of the text being
clipped, as the LaserWriter will not image parts of the page within
about 1/4 inch of the edge.
.space 1
\h3 Head\h3: This is followed by an integer specifying the depth,
in points, of the top and bottom margins of each text page.  The
default margin is 56pt.  Similar remarks apply to very
narrow margins as were given for the \h3 side\h3$$ option.
.space 1
\h3 Landscape\h3: This option specifies that the pages of the
document are to be printed in landscape orientation.  The left, right
top and bottom margins are considered to be relative to the reading
direction of the text on the page, and so, in landscape orientation,
they are rotated along with the text.
.space 1
\h3 Portrait\h3: This option specifies that the pages of the document
are to be printed in portrait orientation (the default).
.space 1
\h3 A4toA5\h3: This option specifies that the document pages are to
be made up as though intended for A4 sheets, but are then to be reduced
to A5 and two such pages printed sideways on each actual A4 sheet.
The reduction takes place after the other page options have taken
effect.  So, the fount size, line spacing, margin dimensions and
orientation all refer to the A4 page before it is reduced to A5, not
to the actual physical attributes of the A5 pages.
.space 1
\h3 A4\h3: This option specifies that the document pages are to be
made up for printing on A4 sheets (the default).
.section Program Operation
Each line of the input document results in a line of text in the
output document.  The beginning of each line is aligned just to the
right of the left-hand side margin.  The line is printed using the
characters of the specified fount, and if the right-hand end of
the line would spill into the right-hand side margin, it is clipped
at that point.  If a line to be printed at the bottom of the page would
fall within the bottom margin, a new page is started and the line is
placed just below the top margin of the next page.  Any ANSI carriage
controls present in the input file are dealt with in the appropriate
manner.
.par
Any errors encountered by the program result in a message being
written on the verification output and the program then stops
immediately, with a return code of 8.  For errors detected in the
option string, the location of the error is indicated.
If no errors are detected,
a summary of the contents of the output document is written on the
verification output.
.section Notes on Use
(1) With the default settings, text that is 80 columns wide will just
fit across the page, and there are 60 lines per page.  Altering the
fount used may change these values.
.space
(2) To be able to fit a full line-printer page (132 columns by 60 lines)
on an A4 sheet, the following settings are recommended:
.display
            LANDSCAPE;FSIZE=8;LSPACE=8;HEAD=54;SIDE=100
.endd
