/********************************************************************
*                                                                   *
*                              PRTOPS                               *
*                                                                   *
*         A program to convert text files, suitable for printing    *
*         on a lineprinter, to PostScript for A4 sheets.            *
*                                                                   *
* Copyright (c) University of Cambridge, 1990, 1994                 *
*                                                                   *
* Author - S. Kearsey, Cambridge University Computing Service       *
* Last Updated - November 1994                                      *
*                                                                   *
*-------------------------------------------------------------------*
* Permission is granted to anyone to use this software for any      *
* purpose on any computer system, and to redistribute it freely,    *
* subject to the following restrictions:                            *
*                                                                   * 
* 1. This software is distributed in the hope that it will be       *
*    useful, but WITHOUT ANY WARRANTY; without even the implied     *
*    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR        *
*    PURPOSE.                                                       *
*                                                                   * 
* 2. The origin of this software must not be misrepresented, either *
*    by explicit claim or by omission.                              *
*                                                                   * 
* 3. Altered versions must be plainly marked as such, and must not  *
*    be misrepresented as being the original software.              *
********************************************************************/
 

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

#define TRUE  1                   /* boolean constants             */
#define FALSE 0

#define STORESIZE 1000            /* internal store size           */

#define CBUFSIZE 64               /* character buffer size         */
#define OBUFSIZE 255              /* output buffer size (bytes)    */

#define MAXSTRINGLEN 255          /* max no of chars in strings    */
#define MAXISEGLEN   252          /* max chars in input rec segment*/
#define MAXOLINELEN  80           /* max no of chars in output line*/

#define DEFAULT_FOUNTSZ 10        /* default parameter values      */
#define DEFAULT_FNAME   "Courier"
#define DEFAULT_LINESP  12
#define DEFAULT_SMARGIN 56
#define DEFAULT_HMARGIN 56

#define T_UNKNOWN 0               /* token values                  */
#define T_NAME    1
#define T_INT     2
#define T_FLOATPT 3
#define T_STRING  4
#define T_EOF     10
#define T_SCOLON  11
#define T_EQUALS  12

#define K_UNKNOWN 0               /* keyword values                */
#define K_FOUNT   1
#define K_FSIZE   2
#define K_LINESP  3
#define K_SMARGIN 4
#define K_HMARGIN 5
#define K_A4TOA5  6
#define K_LANDSCP 7
#define K_PORTRAIT 8
#define K_A4      9

#define A4_WIDTH  594             /* paper dimensions (points)     */
#define A4_HEIGHT 840

#define O_PORTRAIT  0             /* text orientation              */
#define O_LANDSCAPE 1

#define PL_A4  0                  /* page layout                   */
#define PL_2A5 1

typedef struct line {
    char   cc;
    char  text[MAXISEGLEN+1];
    } line;

typedef struct token {
    int   type;
    union { int i;
            char *s;
            char c; } value;
    } token;

/* function declarations */

int  prtops(int,char *[]);
void initialise(int,char *[]);
void getfileargs(int,char *[]);
void get_opts(int, char *[]);
char *get_string(void);
int  get_integer(void);
void header(void);
void prologue(void);
void trailer(void);
void report_info(void);
int  get_file(void);
int  get_line(struct line *);
void line_feed(void);
void carriage_return(void);
void start_page(void);
void end_page(void);
void flush_lastpage(void);
void subpage_header(int);
void subpage_trailer(void);
int  next_subpage(int);
void A4page_header(void);
void A4page_trailer(void);
void new_page(void);
void put_line(struct line);
void convert_char(char,char *);
void itoc(int,char *);
char *newvec(size_t);
void initkeys(void);
void defkey(int,char *);
int  lookupkey(char *);
int  gettok(token *);
void rdname(char *,char);
void rdstring(char *,char);
void rdnumber(int *,char);
int  alphanum(char);
int  readrec(char []);
char uc(char);
void wchars(char *);
void wch(char);
char rch(void);
void unrch(void);
int  rdch(void);
void unrdch(void);
void flush_obuf(void);
void report_context(void);
void report_opterr(char *,int,char *);
void report_error(char *,int);

/* Static variables */

FILE *i_stream = stdin;                /* input file pointer                  */
FILE *o_stream = stdout;               /* output file pointer                 */
FILE *v_stream = stderr;               /* verification file pointer           */

char *c_stream = "";                   /* command input string                */
int  c_ptr = 0;                        /* command input string pointer        */

int pwidth;                            /* global variables                    */
int pheight;
int orientation;
int page_layout;
int font_size;
int line_spacing;
int side_margin;
int head_margin;
char *font_name;
int page_started;
int top_of_page;
int current_x;
int current_y;
int npage;
int page_seq;
int nA4page;
int left_margin;
int right_margin;
int top_margin;
int bottom_margin;

char storevec[STORESIZE];              /* internal storage space              */
char *storep = storevec + STORESIZE;   /* pointer to internal storage         */

char obuf[OBUFSIZE];                   /* output buffer                       */
int  obufp = 0;
int  olinerem = MAXOLINELEN;

char cbuf[CBUFSIZE];                   /* option input buffer                 */
int  cbufp = 0;

char sbuf[MAXSTRINGLEN+1];             /* string buffer                       */


struct keynode {                       /* keyword data structure              */
    struct keynode *link;
    int  value;
    char *text;
    } *keytable = NULL;
int rcode = 0;                         /* return code                         */
char **flist;                          /* list of input files                 */
int nf = 0;                            /* number of files                     */

/* Main body of code */

int main(int argc,char *argv[]) {      /* dummy main function                 */

    return prtops(argc,argv);          /* enter real prtops routine           */
    }

int prtops(int argc,char *argv[]) {    /* prtops main routine                 */
    struct line l;

    initialise(argc,argv);             /* initialise program                  */
    header();                          /* write PostScript header             */
    prologue();                        /* write PostScript prologue           */
    while (get_file()) {               /* open next input file                */
        while (get_line(&l)) {         /* get a line from input file          */
            switch (l.cc) {            /* branch on carriage control          */
              case '-':                /* 2 blank lines                       */
                line_feed();
              case '0':                /* 1 blank line                        */
                line_feed();
              case ' ':                /* next line                           */
              default:
                line_feed();
              case '+':                /* overprint                           */
                carriage_return();
                put_line(l);
                break;
              case '1':                /* next page                           */
                new_page();
                put_line(l);           /* output line                         */
                break;
              }
            }
        }
    trailer();                         /* write PostScript trailer            */
    flush_obuf();                      /* flush out remaining output          */
    report_info();                     /* report document information         */
    return rcode;                      /* return with current return code     */
    }


void initialise(int argc,char *argv[]) { /* perform initialisation            */
    int i;

    for (i=1;i<=CBUFSIZE;i++)          /* clear command buffer                */
        cbuf[i-1] = '\0';
    getfileargs(argc,argv);            /* get list of files                   */

    initkeys();                        /* initialise keywords                 */

    pwidth       = A4_WIDTH;           /* default paper width                 */
    pheight      = A4_HEIGHT;          /* default paper height                */
    orientation  = O_PORTRAIT;         /* default text orientation            */
    page_layout  = PL_A4;              /* default page layout                 */
    font_size    = DEFAULT_FOUNTSZ;    /* default font size                   */
    line_spacing = DEFAULT_LINESP;     /* default linespacing                 */
    side_margin  = DEFAULT_SMARGIN;    /* default side margin                 */
    head_margin  = DEFAULT_HMARGIN;    /* default head margin                 */
    font_name    = newvec(MAXSTRINGLEN);/* default font                       */
    strcpy(font_name,DEFAULT_FNAME);   /* default font name                   */
    page_started = FALSE;              /* page started flag                   */
    top_of_page  = TRUE;               /* first-line-on-page flag             */
    npage    = 0;                      /* current logical page number         */
    page_seq = 1;                      /* page sequence number                */
    nA4page  = 0;                      /* current number of A4 sheets printed */

    get_opts(argc,argv);               /* set up options required             */

    left_margin   = side_margin;           /* left margin position            */
    right_margin  = pwidth - side_margin;  /* right margin position           */
    top_margin    = pheight - head_margin; /* top margin position             */
    bottom_margin = head_margin;           /* bottom margin position          */
    }


void getfileargs(int argc,char *argv[]) {  /* get file list from cmnd args    */
    int endopt = 1;
    int i;

    for (i=argc-1;i>=1;i--) {              /* work back along args            */
        if (*(argv[i])=='-' && strlen(argv[i]) != 1) { /* if get to option keyword */
            endopt = i+2;                  /* point to first file beyond it   */
            break;                         /* stop loop                       */
            }
        }
    nf = argc - endopt;                    /* number of file args             */
    if (nf>0) {
        flist = (char **) malloc((size_t) sizeof(argv[0])*nf); /* get store for list */
        if (flist==NULL) {                 /* can't get store for list        */
            report_error("insufficient store available",8);
            }
        for (i=0;i<nf;i++) {               /* copy list of file args          */
            flist[i] = argv[endopt+i];
            }
        }
    }


void get_opts(int argc,char *argv[]) {     /* fetch options                   */
    char opt;
    int i;
    token tok;

    for (opt=getopt(argc,argv,"p:f:");!(opt==EOF);) { /* for each parameter   */
        switch (opt) {                     /* branch on x                     */
          case 'p':                        /* '-p'                            */
            c_stream = optarg;             /* point to argument string        */
            break;
          default:                         /* everything else - do nothing    */
            break;
            }
        opt = getopt(argc,argv,"p:f:");    /* get next parameter              */
        }
    while (! (gettok(&tok)==T_EOF)) {      /* get next token until EOF        */
        switch (tok.type) {                /* branch on token type            */
          case T_NAME: {                   /* alphameric name                 */
            char *name = tok.value.s;      /* pointer to name                 */
            int key = lookupkey(name);     /* look it up in keyword table     */

            switch (key) {                 /* branch on key value             */
              case K_FOUNT:                /* set fount used                  */
                strcpy(font_name,get_string());/* get fount name              */
                break;
              case K_FSIZE:                /* set fount size                  */
                font_size = get_integer();
                break;
              case K_LINESP:               /* set line spacing                */
                line_spacing = get_integer();
                break;
              case K_SMARGIN:              /* set side margins                */
                side_margin = get_integer();
                break;
              case K_HMARGIN:              /* set head and tail margins       */
                head_margin = get_integer();
                break;
              case K_A4:                   /* A4 page on A4 sheet             */
                page_layout = PL_A4;
                break;
              case K_A4TOA5:               /* two A5 pages on a A4 sheet      */
                page_layout = PL_2A5;
                break;
              case K_LANDSCP:              /* landscape orientation           */
                orientation = O_LANDSCAPE;
                pwidth = A4_HEIGHT;        /* reset paper dimensions          */
                pheight = A4_WIDTH;
                break;
              case K_PORTRAIT:             /* portrait orientation            */
                orientation = O_PORTRAIT;
                pwidth = A4_WIDTH;         /* reset paper dimensions          */
                pheight = A4_HEIGHT;
                break;
              case K_UNKNOWN: default:     /* unknown keyword                 */
                report_opterr("unknown option word %s",8,name);
                break;
                }
            }
            break;
          case T_SCOLON:                   /* ignore semi-colons              */
            break;
          default:                         /* anything else                   */
            report_opterr("word expected",8,NULL); /* report option error     */
            break;
            }
        }
    }


char *get_string() {                       /* gets string argument            */
    char *str;
    token tok;
    int ttype = gettok(&tok);              /* next token                      */

    if (ttype == T_EQUALS) ttype = gettok(&tok); /* skip '='                  */
    if (ttype == T_STRING) {               /* if a string                     */
        str = tok.value.s;                 /* extract pointer                 */
        }
    else {                                 /* if not                          */
        report_opterr("string expected",8,NULL);/* report error               */
        }
    return str;                            /* return pointer to string        */
    }


int get_integer() {                        /* gets integer argument           */
    int num = 0;
    token tok;
    int ttype = gettok(&tok);              /* next token                      */

    if (ttype == T_EQUALS) ttype = gettok(&tok); /* skip '='                  */
    if (ttype == T_INT) {                  /* if a integer                    */
        num = tok.value.i;                 /* extract value                   */
        }
    else {                                 /* if not                          */
        report_opterr("number expected",8,NULL);/* report error               */
        }
    return num;                            /* return integer value            */
    }


void initkeys() {                      /* set up table of option keywords     */
    struct keynode *node;

    defkey(K_FOUNT,"FOUNT");           /* define keywords                     */
    defkey(K_FOUNT,"FONT");
    defkey(K_FSIZE,"FSIZE");
    defkey(K_LINESP,"LSPACE");
    defkey(K_SMARGIN,"SIDE");
    defkey(K_HMARGIN,"HEAD");
    defkey(K_A4,"A4");
    defkey(K_A4TOA5,"A4TOA5");
    defkey(K_LANDSCP,"LANDSCAPE");
    defkey(K_PORTRAIT,"PORTRAIT");
    }


void defkey(int value, char *text) {   /* enter key into key table            */
    struct keynode *node;              /* pointer to node                     */

    node = (struct keynode *) newvec(sizeof(struct keynode)); /* get store for new node */
    node->link = keytable;             /* point to current beginning of table */
    node->value = value;               /* enter key value                     */
    node->text = (char *) newvec(strlen(text)+1); /* get store for text and point to it */
    strcpy(node->text,text);           /* copy text of key to node            */
    keytable = node;                   /* new node is now beginning of table  */
    }


int lookupkey(char *text) {            /* look up key value in key table      */
    struct keynode *node = keytable;   /* first node in table                 */
    int nchars = 0;
    int match  = FALSE;
    char *ktext;
    int i;

    while (node != NULL && !match) {   /* until at end of table or key match  */
        ktext = node->text;            /* point to key text                   */
        nchars = strlen(ktext);        /* number of chars in key              */
        if (strlen(text) == nchars) {  /* if key and text are same size       */
            match = TRUE;              /* there is a potential match          */
            for (i=0;i<nchars;i++) {   /* for each character in key           */
                if (uc(text[i]) != ktext[i]) { /* if uppercased char doesn't match */
                    match = FALSE;     /* no match                            */
                    node = node->link; /* get next node                       */
                    break;             /* stop checking current key           */
                    }
                }
            }
        else {                         /* key and text different lengths      */
            node = node->link;         /* get next node                       */
            }
        }
    if (node == NULL) return K_UNKNOWN; /* if no match return K_UNKNOWN       */
    return node->value;                /* return key value                    */
    }


int gettok(token *tok) {               /* get next lexical token from input   */
    int type = T_UNKNOWN;              /* token type                          */
    int value = 0;                     /* token value                         */
    char *str = sbuf;                  /* buffer for strings                  */
    int num = 0;                       /* numerical values                    */
    char ch = rch();                   /* get next character of input         */

    while (ch==' ' || ch=='\f' || ch=='\n' || ch=='\t') ch = rch(); /* skip spaces,newpage,newline and tabs */
    switch (uc(ch)) {                  /* branch on character (uppercased)    */
      case 'A': case 'B': case 'C': case 'D': case 'E':
      case 'F': case 'G': case 'H': case 'I': case 'J':
      case 'K': case 'L': case 'M': case 'N': case 'O':
      case 'P': case 'Q': case 'R': case 'S': case 'T':
      case 'U': case 'V': case 'W': case 'X': case 'Y':
      case 'Z':
        rdname(str,ch);                /* read in an alphanumeric name        */
        tok->type = T_NAME;            /* name token                          */
        tok->value.s = str;            /* token value is pointer to string    */
        break;
      case '0': case '1': case '2': case '3': case '4': /* digit 0-9          */
      case '5': case '6': case '7': case '8': case '9':
        rdnumber(&num,ch);             /* read a number                       */
        tok->type = T_INT;             /* set up token                        */
        tok->value.i = num;
        break;
      case '\"': case '\'':            /* string delimiter                    */
        rdstring(str,ch);              /* read a string                       */
        tok->type = T_STRING;
        tok->value.s = str;
        break;
      case ';':                        /* semicolon                           */
        tok->type = T_SCOLON;
        tok->value.c = ch;
        break;
      case '=':                        /* equals                              */
        tok->type = T_EQUALS;
        tok->value.c = ch;
        break;
      case EOF:                        /* end of input stream                 */
        tok->type = T_EOF;
        tok->value.c = ch;
        break;
      default:                         /* anything else is unrecognised       */
        tok->type = T_UNKNOWN;
        tok->value.c = ch;
        break;
        }
    return tok->type;                  /* return with type of token found     */
    }


void rdname(char *str,char ch1) {      /* read a word of alphanumeric chars   */
    int ptr = 1;
    char ch = rch();                   /* get next character                  */

    str[0] = ch1;                      /* put first character away            */
    while (alphanum(ch)) {             /* while next char is alphanumeric     */
        str[ptr] = ch;                 /* put character away                  */
        ptr += 1;                      /* increment string offest             */
        ch = rch();                    /* get next character                  */
        }
    unrch();                           /* put non-alphanumeric char back      */
    str[ptr] = '\0';                   /* terminate string                    */
    }


void rdstring(char *str,char dlm) {    /* reads a string                      */
    int ptr = 0;
    char ch = rch();                   /* get next character                  */

    while (! (ch==dlm || ch=='\n' || ch==EOF)) { /* until delimiter or eof/eol*/
        str[ptr] = ch;                 /* put character in string             */
        ptr += 1;                      /* increment string offset             */
        ch = rch();                    /* get next character                  */
        }
    str[ptr] = '\0';                   /* terminate string                    */
    }


void rdnumber(int *num_addr,char ch1) {/* reads an integer number             */
    int done = FALSE;
    int d = 0,num = 0;
    char ch = ch1;

    while (!done) {                    /* repeat until done                   */
        switch (ch) {                  /* branch on character                 */
          case '0': case '1': case '2': case '3': case '4': /* digit 0-9      */
          case '5': case '6': case '7': case '8': case '9':
            d = ch - '0';              /* numerical equivalent of digit       */
            num = num*10 + d;          /* add to current partial result       */
            ch = rch();                /* get next character                  */
            break;
          default:                     /* anything else                       */
            unrch();                   /* put terminating char back           */
            done = TRUE;               /* stop getting characters             */
            break;
            }
        }
    *num_addr = num;                   /* return number                       */
    }


int alphanum(char ch) {                /* tests if ch is alphameric           */

    if (isalnum(ch)) return TRUE;      /* returns TRUE  if alphameric         */
    else return FALSE;                 /*         FALSE if not                */
    }



void header() {                        /* PostScript file header              */

    wchars("%!PS-Adobe-2.0\n");
    wchars("%!needs a4\n");            /* local convention (used by PSOUT)    */
    wchars("%%Title: Line Printer File\n");
    wchars("%%Creator: prtops\n");
    wchars("%%Pages: (atend)\n");
    wchars("%%EndComments\n");
    }


void prologue() {                      /* PostScript prologue -               */
    char str[64];                      /*     definitions and set-up          */

    wchars("a4\n");                    /* A4 fix-up (origin btm lft corner)   */
/*  wchars("statusdict /waittimeout 200 put\n");  /* lengthen timeouts for line delays */
    strcpy(str,"/");
    strcat(str,font_name);
    strcat(str," findfont ");
    wchars(str);                       /* find fount requested                */
    itoc(font_size,str);
    strcat(str," scalefont setfont\n");
    wchars(str);
    wchars("/mt {moveto} def /s {show} def\n"); /* aliases for operations used */
    wchars("/window { /y2 exch def /y1 exch def /x2 exch def /x1 exch def\n"); /* define window operation */
    wchars("newpath x1 y1 moveto x2 y1 lineto x2 y2 lineto x1 y2 lineto\n"); /* path round border */
    wchars("closepath clip} def\n");
    wchars("%%EndProlog\n");
    }


void trailer() {                       /* PostScript Trailer                  */
    char str[64];
    char num[16];

    if (page_started) flush_lastpage(); /* end current A4 page if started     */
    wchars("%%Trailer\n");
    strcpy(str,"%%Pages: ");           /* build string "%%Pages: n\n"         */
    itoc(nA4page,num);                 /* total number of pages in document   */
    strcat(str,num);
    strcat(str,"\n");
    wchars(str);
    }


void subpage_header(int seq) {         /* PostScript for subpage start        */
    char str[16];
    int page_offset = 0;

    switch (page_layout) {             /* branch on page layout               */
      case PL_A4:                      /* single A4 pages                     */
        page_offset = 0;               /* no offest to page (could add        */
        break;                         /*            odd/even offsets later ) */
      case PL_2A5:                     /* two A5 pages on an A4 sheet         */
        switch (seq) {                 /* branch on page sequence number      */
          case 1:                      /* first subpage on sheet              */
            page_offset = 0;           /* no offset                           */
            break;
          case 2:                      /* second subpage on sheet             */
            page_offset = A4_WIDTH;    /* offset by A4 page width (pre-scaled */
            break;                     /*                              units) */
          }
        break;
      }
    wchars("save\n");                  /* save current VM state               */
    itoc(page_offset,str); wchars(str);/* apply page offset                   */
    wchars(" 0 translate\n");
    if (orientation==O_LANDSCAPE) {    /* if landscape orientation            */
       itoc(pheight,str); wchars(str); /* rotate user coordinates             */
       wchars(" 0 translate [0 1 -1 0 0 0] concat\n");
       }                               /* [0 1 -1 0 0 0] matrix preferred
                                          because more accurate - allows
                                          LaserWriter to recognise aligned
                                          clipping rectangle in device
                                          coordinates and use optimised
                                          clipping algorithms (five times
                                          faster).                            */
    itoc(left_margin,str); wchars(str); wch(' '); /* set up windowing at margins */
    itoc(right_margin,str); wchars(str); wch(' ');
    itoc(bottom_margin,str); wchars(str); wch(' ');
    itoc(top_margin,str); wchars(str); wch(' ');
    wchars("window\n");
    }


void subpage_trailer() {               /* PostScript for subpage end          */

    wchars("restore\n");               /* restore VM state                    */
    }


int next_subpage(int seq) {            /* returns next sequence number        */
    int next = 0;

    switch (page_layout) {             /* branch on page layout               */
      case PL_A4:                      /* single A4 pages                     */
        next = 1;                      /* always first on page                */
        break;
      case PL_2A5:                     /* two A5 pages on an A4 sheet         */
        next = (seq % 2) + 1;          /* cycle through [1],[2]               */
        break;
      }
    return next;                       /* return seq number of next page      */
    }


void A4page_header() {                 /* PostScript for A4 page start        */
    char str[16];

    wchars("%%Page:? ");               /* sheet number                        */
    itoc(nA4page,str); wchars(str); wch('\n');
    if (page_layout==PL_2A5) {         /* if printing A4 on A5                */
        itoc(A4_WIDTH,str); wchars(str); /* use landscape                     */
        wchars(" 0 translate [0 1 -1 0 0 0] concat\n"); /* matrix preferred
                                            (see subpage_header)              */
        wchars("1 2 sqrt div dup scale\n"); /* and scale by 1/sqrt(2.0)       */
        }
    }


void A4page_trailer() {                /* PostScript for page end             */

    wchars("showpage\n");              /* print page                          */
    }


int get_file() {                       /* selects next input file             */
    static int file = 0;
    int done = FALSE;

    if (file==0 || file<nf) {          /* if first time, or all files not done*/
        if (nf==0) {                   /* if no files, use stdin              */
            i_stream = stdin;
            }
        else if (strlen(flist[file])==1 && *flist[file]=='-') { /* if '-' use stdin */
            i_stream = stdin;
            }
        else {
            i_stream=fopen(flist[file],"r"); /* open file                     */
            }
        if (i_stream==NULL) report_error("no input file",8);
        file++;
        done = TRUE;
        }
    return done;                       /* returns TRUE if file opened         */
    }                                  /*         FALSE if not                */


void new_page() {                      /* moves to next page               */

    if (page_started) end_page();      /* end current page if there is one */
    top_of_page = TRUE;                /* set first-line flag              */
    }


int get_line(struct line *l) {         /* get line of input text        */
    static int seg_no = 0;             /* record segment count          */
    static int wrap_warn_flag = FALSE; /* wrapping flag                 */
    int eol = FALSE;                   /* end-of-line-obtained flag     */
    char rec[MAXISEGLEN+1];            /* input buffer                  */
    int nchars = 0, start  = 0;

    nchars = readrec(rec);             /* read record                   */
    if (nchars<-1 && wrap_warn_flag==FALSE) { /* first time record segmented */
        report_error("overlong input lines wrapped",4); /* issue warning  */
        wrap_warn_flag = TRUE;
        }
    if (nchars==-1) return FALSE;      /* if end-of-file, return        */
    if (nchars>=0) eol = TRUE;         /* if end-of-line segment, set flag */
    nchars = abs(nchars);              /* make nchars positive          */
    seg_no += 1;                       /* increment segment count       */
    if (seg_no==1 && nchars>0) {       /* if 1st seg and not empty      */
        l->cc = rec[0];                /* pick off carriage control     */
        nchars -= 1;                   /* number of chars in text       */
        start = 1;                     /* offset to start of text       */
        }
    else {                             /* not the first segment         */
        l->cc = ' ';                   /* wrap to next line             */
        start = 0;                     /* offset to start of text       */
        }
    if (eol==TRUE) seg_no = 0;         /* reset seg count if eol obtained */
    strcpy(l->text,rec+start);         /* copy record text to line      */
    return TRUE;
    }


void put_line(struct line l) {         /* put line out (convert to PostScript)*/
    char str[16];
    char ch = '\0';
    int i;

    if (top_of_page) {                 /* if first line on page             */
        start_page();                  /* put out PostScript page header    */
        top_of_page = FALSE;           /* unset flag                        */
        }
    if (strlen(l.text)>0) {            /* if line not empty                 */
        itoc(current_x,str); wchars(str); wch(' '); /* convert x to string    */
        itoc(current_y,str); wchars(str); wch(' '); /* convert y to string    */
        wchars("mt (");                /* move and begin string             */
        for (i=0;i<strlen(l.text);i++) { /* for each character                */
            ch = l.text[i];            /* extract character                 */
            convert_char(ch,str);      /* convert character to PS string    */
            if (olinerem<=strlen(str)) { /* if last char or escaped sequence  */
                wchars("\\\n");        /* nullify newline with an escape '\'*/
                }
            wchars(str);               /* write character (or sequence) out */
            }
        wchars(") s\n");               /* end string and set - newline      */
        }
    }


void line_feed() {                     /* move down a line             */

    if (!top_of_page) {                /* ignore if at top of new page */
       current_y -= line_spacing;      /* move current y position down */
       if ((current_y-line_spacing/2)<bottom_margin) { /* if off bottom  */
          new_page();                  /* go to next page              */
          }
       }
    }


void carriage_return() {               /* move to left margin of page */

    current_x = left_margin;           /* still on same line          */
    }


void start_page() {                    /* start a new page off              */

    page_started = TRUE;               /* a page has now been started       */
    npage += 1;                        /* increment page counter            */
    current_x = left_margin;           /* move to upper left corner of page */
    current_y = top_margin - line_spacing;
    if (page_seq==1) {                 /* if first page on A4 sheet         */
        nA4page += 1;                  /* increment A4 sheet count          */
        A4page_header();               /* write PostScript A4 page header   */
        }
    subpage_header(page_seq);          /* write PostScript subpage header   */
    }


void end_page() {                      /* end current page                 */

    subpage_trailer();                 /* write PostScript subpage trailer */
    page_seq = next_subpage(page_seq); /* get sequence number of next page */
    if (page_seq == 1) {               /* if next starts a new A4 sheet    */
       A4page_trailer();               /* write PostScript A4 page trailer */
       }
    }


void flush_lastpage() {                /* end last subpage and flush A4 sheet */

    subpage_trailer();                 /* write PostScript subpage trailer    */
    A4page_trailer();                  /* write PostScript A4 page trailer    */
    }


int readrec(char rbuf[]) {       /* read in a record from the input stream    */
    static char cc = ' ';        /* sets lst char in rbuf to carriage control */
    static int seg = FALSE;      /*         space   -   newline               */
    int done = FALSE;            /*           +     -   carriage return       */
    char ch;                     /*           1     -   newpage               */
    int n=0;

    if (!seg) {                        /* if start of record            */
        rbuf[0]=cc;                    /* put in cc                     */
        rbuf[1]='\0';                  /* terminate the string          */
        n = 1;                         /* increment buffer pointer      */
        }
    while (!done) {                    /* until finished                */
        if (n==MAXISEGLEN) {           /* if buffer full                */
            n = -n;
            seg = TRUE;
            break;                     /* stop reading                  */
            }
        ch = fgetc(i_stream);          /* read next character           */
        switch (ch) {                  /* branch on character           */
          case '\r': case '\n': case '\f':   /* <cr> <lf> <ff>                */
            if (ch=='\r') cc = '+';          /* save cc for next line         */
            if (ch=='\n') cc = ' ';
            if (ch=='\f') cc = '1';
            seg = FALSE;               /* end of segmentation           */
            done = TRUE;
            break;
          case EOF:                    /* end of file or error          */
            if (n>1) {                 /* if characters already read    */
                cc = ' ';              /* set up cc for 1st line of next file */
                }
            else {                     /* bare eof                       */
                if (feof(i_stream)) n = -1; /* if real eof                    */
                if (ferror(i_stream))  /* if input error                 */
                    report_error("error reading input file",8);
                }
            seg = FALSE;
            done = TRUE;
            break;
          default:                     /* data characters                */
            rbuf[n] = ch;              /* put char in buffer             */
            rbuf[n+1] = '\0';          /* terminate string               */
            n += 1;                    /* increment buffer pointer       */
            break;
          }
        }
    return n;                          /* return n>=0 end of record read */
    }                                  /*        n=-1 end of file        */
                                       /*        n<-1 record segmented   */

char uc(char c) {                      /* uppercase any letters (a-z) */

    if (islower(c))                    /* if lowercase letter */
        return toupper(c);             /* return uppercased letter */
    else
        return c;                      /* return character untouched */
    }


void wlex(char *str) {                 /* outputs a lexeme (must not wrap) */

    if (olinerem<strlen(str)) {        /* if string too long for rest of line */
        wch('\n');                     /* start new line */
        }
    wchars(str);                       /* output string */
    }


void wchars(char *str) {               /* outputs a string of characters */
    int i=0;                           /* they may be wrapped to next line */

    while (str[i]!='\0') {             /* output each character */
        wch(str[i++]);
        }
    }


void wch(char ch) {                    /* outputs a character */

    if (ch == '\n') {                  /* if newline character */
        olinerem = MAXOLINELEN;        /* reset line remaining count */
        }
    else {                             /* if anything else */
        if (olinerem == 0) {           /* if current line full */
            wch('\n');                 /* start a new line */
            }
        else {                         /* if line not full */
            olinerem -= 1;             /* decrement char remaining count */
            }
        }
    if (obufp>=OBUFSIZE-1) {           /* if output buffer full */
        flush_obuf();                  /* write buffer out */
        }
    obuf[obufp] = ch;                  /* add char to buffer */
    obufp += 1;                        /* increment buffer pointer */
    obuf[obufp] = '\0';                /* make sure it's null terminated */
    }


void flush_obuf() {                    /* flush output buffer */

    fputs(obuf,o_stream);              /* write out buffer contents */
    obuf[0] = '\0';                    /* empty buffer */
    obufp = 0;
    }


void convert_char(char ch,char *str) { /* make char suitable for PS string    */
    int offset = 0;

    switch (ch) {                      /* branch on character                 */
      case '\b':                       /* backspace change to \b              */
        strcpy(str,"\\b");
        break;
      case ')': case '(': case '\\':   /* (,),\ become \(,\),\\               */
        str[0] = '\\';                 /* add '\'                             */
        offset += 1;
      default:                         /* all the rest                        */
	if ((unsigned char)ch <= 127) {/* 7 bit characters                    */
          str[offset] = ch;            /* add character to string             */
          str[offset+1] = '\0';        /* terminate string with null          */
	  }
	else {                         /* 8 bit characters                    */
	  sprintf(str,"\\%o",(unsigned char)ch); /* change to \nnn            */
	  }
        break;
/*    case '\243':                     /* pound sign                          */
/*      strcpy(str,"\\243");           /* change to \243                      */
/*      break; */
      }
    }


void itoc(int i,char *s) {             /* convert int to decimal char format  */

    sprintf(s,"%d",i);
    }


char *newvec(size_t n) {           /* allocate new object from internal store */

    n = ((n+3)/4)*4;                   /* round up to nearest multiple of 4   */
    storep = storep - n;               /* move pointer to first byte of object*/
    if (storep < storevec) {           /* if off beginning of store array     */
        report_error("Insufficient internal store",8); /* give error          */
        }
    return storep;                     /* return pointer to new object        */
    }


char rch() {                           /* read next character of command input*/
    char ch = rdch();                  /* read character                      */

    cbuf[cbufp] = ch;                  /* store char in buffer                */
    cbufp += 1;                        /* increment pointer in buffer         */
    cbufp = cbufp % CBUFSIZE;          /* if off end - back to beginning      */
    return ch;                         /* return character                    */
    }


void unrch() {                         /* put character back on input         */

    unrdch();                          /* unread character                    */
    cbufp -= 1;                        /* move buffer pointer back            */
    if (cbufp<0) cbufp += CBUFSIZE;    /* if off front move to end            */
    }


int rdch() {                           /* read character                      */
    int ch;

    ch = c_stream[c_ptr];              /* next character in buffer            */
    if (ch=='\0') ch = EOF;            /* if end of string, send EOF          */
    c_ptr += 1;                        /* increment buff ptr                  */
    return ch;                         /* return char                         */
    }


void unrdch() {                        /* put character back on input         */

    if (c_ptr>0) c_ptr--;              /* if positive - decrement pointer     */
    }


void report_info() {                   /* report info about output doc.       */
    char *page_size = "A4";

    if (page_layout==PL_2A5) page_size = "A5";
    fprintf(v_stream,                                                          \
          "Output: %d %s page(s) on %d A4 sheet(s), %dpt %s, %dpt spacing.\n", \
          npage,page_size,nA4page,font_size,font_name,line_spacing);
    }


void report_context() {                /* report context of syntax error      */
    char ch = '\0';
    int p = cbufp+1,i;                 /* start beyond possible unrch() char  */

    for (i=1;i<CBUFSIZE;i++) {         /* for each character in buffer        */
        if (p>=CBUFSIZE) p -= CBUFSIZE;/* if off end move to beginning        */
        ch = cbuf[p];                  /* get character                       */
        if (ch!=0) {                   /* unless not read yet                 */
            fputc(ch,v_stream);        /* write out character                 */
            if (ch!='\n' && ch!=' ' && ch!='\f' && ch!='\b' && ch!='\t')
                cbuf[p] = ' ';         /* replace with space if not ctrl char */
            }
        p++;                           /* point to next character             */
        }
    fputc('\n',v_stream);
    cbuf[cbufp-1] = '^';               /* indicate last character with ^      */
    p = cbufp+1;
    for (i=1;i<CBUFSIZE;i++) {         /* put out pointer to last char read   */
        if (p>=CBUFSIZE) p -= CBUFSIZE;
        ch = cbuf[p];
        if (ch!=0) fputc(ch,v_stream);
        p++;
        }
    fputc('\n',v_stream);
    }


void report_opterr(char *msg,int rc,char *s) { /* report syntax errors in opts*/

    if (rc>rcode) rcode = rc;          /* accumulate max rcode                */
    if (rcode>4) {                     /* if error                            */
        fprintf(v_stream,"****PRTOPS Error - ");
        fprintf(v_stream,msg,s);       /* put out error message               */
        fprintf(v_stream,"\n");
        report_context();              /* report context of error             */
        fprintf(v_stream,"PRTOPS abandoned\n");
        exit(rcode);                   /* exit with current return code       */
        }
    else {                             /* if only warning                     */
        fprintf(v_stream,"****PRTOPS Warning - "); /* put out warning message */
        fprintf(v_stream,msg,s);       /* put out error message               */
        fprintf(v_stream,"\n");
        report_context();              /* report context of error             */
        }
    }


void report_error(char *message,int rc) {  /* report error                    */

    if (rc>rcode) rcode = rc;              /* accumulate max rcode            */
    if (rcode>4) {                         /* if error                        */
        fprintf(v_stream,"****PRTOPS Error - %s\n",message); /* put out error message */
        fprintf(v_stream,"PRTOPS abandoned\n");
        exit(rcode);                       /* exit with current return code   */
        }
    else {                                 /* if only warning                 */
        fprintf(v_stream,"****PRTOPS Warning - %s\n",message); /* put out warning message */
        }
    }
